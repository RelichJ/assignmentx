This is a group project for my Advanced Java Programming class. It is an implementation of Shogi (Japanese Chess) in Java using the Java Swing API.
---
Given that this application was built a week before finals, there are numerous bugs and performance issues. Most notably the application is hardcoded for 1080p monitors as we could not get the board grid to resize properly.

I do plan on coming back to this project sometime in the future.